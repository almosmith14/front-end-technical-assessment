import axios from "axios";

import { IComment } from "../entities/comment"


const getComments = (org: string) => {
    return (dispatch: Function) => {
        dispatch(fetchComments())
        return axios.get("http://localhost:1337/orgs/" + org + "/comments").then((response) => {
            console.log(response)
            dispatch(setComments(response.data))
        }).catch((error: any) => {
            dispatch(errorComment("GET"))
        })
    }
}

const fetchComments = () => {
    return {
        type: "FETCH_COMMENTS"
    }
}

const setComments = (comments: IComment[]) => {
    return {
        type: "SET_COMMENTS",
        comments
    }
}

const postComments = (comment: string, org: string) => {
    return (dispatch: Function) => {
        return axios.post("http://localhost:1337/orgs/" + org + "/comments", { comment }).then((response) => {
            dispatch(getComments(org));
        }).catch((error: any) => {
            dispatch(errorComment("POST"))
        });
    }
}

const removeComments = (org: string) => {
    return (dispatch: Function) => {
        return axios.delete("http://localhost:1337/orgs/" + org + "/comments").then((response) => {
            dispatch(getComments(org));
        }).catch();
    }
}

const errorComment = (method: string) => {
    return {
        type: "ERROR_COMMENTS",
        method: method
    }
}


export { getComments, postComments, removeComments };