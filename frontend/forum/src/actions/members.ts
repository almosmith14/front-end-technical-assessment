import axios from "axios";
import { error } from "console";
import { IMember } from "../entities/member"

const getMembers = (org: string) => {
    return (dispatch: Function) => {
        dispatch(fetchMembers());
        return axios.get("http://localhost:1337/orgs/" + org + "/members").then((response) => {
            console.log(response)
            dispatch(setMembers(response.data))
        }).catch((error: any) => {
            dispatch(errorMembers())
        })
    }
}

const fetchMembers = () => {
    return {
        type: "FETCH_MEMBERS"
    }
}


const setMembers = (members: IMember[]) => {
    return {
        type: "SET_MEMBERS",
        members
    }
}

const errorMembers = () => {
    return {
        type: "ERROR_MEMBERS"
    }
}


export default getMembers;