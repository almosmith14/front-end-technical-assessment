
const setOrganisation = (organisation: string) => {
    return {
        type: "SET_ORGANISATION",
        org: organisation
    }
}

export default setOrganisation;