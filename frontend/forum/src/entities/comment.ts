export interface IComment {
    _id: string,
    deleted: boolean,
    comment: string,
    org: string,
    createdAt: Date,
    updatedAt: Date,
    __v: number
}