export interface IMember {
    avatar: string,
    followers: number,
    following: number,
    _id: string,
    email: string,
    org: string,
    createdAt: Date,
    updatedAt: Date,
    __v: number
}