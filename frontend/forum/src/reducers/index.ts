import { combineReducers } from "redux";
import commentReducer from "./comments";
import memberReducer from "./members";
import organisationsReducer from "./organisation";

const allReducers = combineReducers({
    members: memberReducer,
    comments: commentReducer,
    org: organisationsReducer
})

export default allReducers