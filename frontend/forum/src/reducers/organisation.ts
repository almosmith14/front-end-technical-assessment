

const organisationsReducer = (state = 'fsociety', action: any) => {
    switch (action.type) {
        case "SET_ORGANISATION":
            return action.org
        default:
            return state
    }
}

export default organisationsReducer;