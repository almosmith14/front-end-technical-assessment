import { notification } from "antd";

const initialState = {
    isLoading: false,
    members: []
}

const membersReducer = (state = initialState, action: any) => {
    switch (action.type) {
        case "FETCH_MEMBERS":
            return { ...state, isLoading: true };
        case "SET_MEMBERS":
            return { ...state, members: action.members, isLoading: false }
        case "ERROR_MEMBERS":
            notification.error({
                message: 'Could not load members',
            });
            return { ...state, members: [], isLoading: false }
        default:
            return { ...state }
    }
}

export default membersReducer;