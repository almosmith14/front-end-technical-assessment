import { notification } from "antd";

const initialState = {
    isLoading: false,
    comments: []
}

const commentReducer = (state = initialState, action: any) => {
    switch (action.type) {
        case "FETCH_COMMENTS":
            return { ...state, isLoading: true };
        case "SET_COMMENTS":
            return { isLoading: false, comments: action.comments };
        case "ERROR_COMMENTS":
            notification.error({
                message:  action.method == 'POST' ? 'Could not post new comment' : 'Could not load comments' ,
            });
            return { ...state, members: [], isLoading: false }
        default:
            return { ...state };
    }
}

export default commentReducer;