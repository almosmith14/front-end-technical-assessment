import { Card, Col, Row, Space, Comment, Tooltip, Button, Form, Empty, Popconfirm, Spin, PageHeader } from "antd";
import TextArea from "antd/lib/input/TextArea";
import moment from "moment";
import { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { getComments, postComments, removeComments } from "../actions/comments";
import { IComment } from "../entities/comment";
import { DeleteOutlined } from '@ant-design/icons';

const Comments = () => {

    const dispatch = useDispatch();
    const comments = useSelector((state: any) => state.comments.comments);
    const loadingComments = useSelector((state: any) => state.comments.isLoading);
    const org = useSelector((state: any) => state.org);

    const [newComment, setNewComment] = useState('');
    const [submitting, setSubmitting] = useState(false);

    useEffect(() => {
        dispatch(getComments(org));
    }, [org])

    const onSubmit = () => {
        setSubmitting(true);
        if (newComment != '') {
            dispatch(postComments(newComment, org));
            setNewComment('');
        }
        setSubmitting(false);
    }

    function confirmDelete() {
        dispatch(removeComments(org));
    }

    if (loadingComments) {
        return (
            <div style={{ textAlign: 'center' }}>
                <Space size="middle">
                    <Spin tip="Loading comments..." size="large" />
                </Space>
            </div>
        )
    }

    return (
        <Row>
            <Col span={24}>
                <Popconfirm
                    title="Are you sure to delete all comments?"
                    onConfirm={confirmDelete}
                    okText="Yes"
                    cancelText="No"
                >
                    <Button type="primary" danger icon={<DeleteOutlined />}>
                        Delete Comments
                    </Button>
                </Popconfirm>



                {comments.length > 0 ?

                    comments.map((comment: IComment, index: number) => {
                        return <CommentComp comment={comment} index={index} />
                    }) : <Empty description="No Comments" />}

                <Editor value={newComment} submitting={submitting} onSubmit={() => onSubmit()} onChange={(value: string) => setNewComment(value)} />
            </Col>
        </Row>

    )
}

const CommentComp = ({ comment, index }: { comment: IComment, index: number }) => {
    return (
        <Comment
            key={index}
            author={comment.org}
            content={
                <p>
                    {comment.comment}

                </p>
            }
            datetime={
                <Tooltip title={moment(comment.updatedAt).format('HH:mm, DD MMMM YYYY')}>
                    <span>{moment(comment.updatedAt).fromNow()}</span>
                </Tooltip>
            }
        />
    )
}

const Editor = ({ submitting, value, onChange, onSubmit }: { submitting: boolean, value: string, onChange: Function, onSubmit: Function }) => (
    <Form style={{ marginTop: 10 }}>
        <Form.Item>
            <TextArea rows={4} onChange={(e) => onChange(e.target.value)} value={value} />
        </Form.Item>
        <Form.Item>
            <Button htmlType="submit" loading={submitting} onClick={() => onSubmit()} type="primary">
                Add Comment
            </Button>
        </Form.Item>
    </Form>
);

export default Comments;