import { Card, Col, Empty, Row, Space, Spin, Statistic } from "antd";
import moment from "moment";
import { useEffect } from "react";
import { useSelector, useDispatch } from "react-redux";
import getMembers from "../actions/members";
import { IMember } from "../entities/member";

const Members = () => {
    const dispatch = useDispatch();
    const members = useSelector((state: any) => state.members.members);
    const loadingMembers = useSelector((state: any) => state.members.isLoading);
    const org = useSelector((state: any) => state.org);

    useEffect(() => {
        dispatch(getMembers(org));
    }, [org])

    if (loadingMembers) {
        return (
            <div style={{ textAlign: 'center' }}>
                <Space size="middle">
                    <Spin tip="Loading members..." size="large" />
                </Space>
            </div>
        )
    }

    return (
        <Row>
            <Col span={24} style={{ textAlign: 'center' }}>
                <Space>
                    {members.length > 0 ? members.map((member: IMember) => {
                        return MemeberCard(member)
                    }) : <Empty description="No Members" />}
                </Space>
            </Col>
        </Row>
    )
}

const MemeberCard = (member: IMember) => {
    return (
        <Card key={member._id} style={{ width: 300 }} cover={
            <img
                alt={member.email}
                src={member.avatar}
            />
        }>
            <Row>
                <Col span={12}>
                    <Statistic
                        title="Followers"
                        value={member.followers}
                        precision={0}
                        valueStyle={{ color: '#1890ff' }}
                    />
                </Col>
                <Col span={12}>
                    <Statistic
                        title="Following"
                        value={member.following}
                        precision={0}
                        valueStyle={{ color: '#1890ff' }}
                    />
                </Col>
            </Row>
            <h4>{member.email}</h4>
            <p>{member.org}</p>
            <small>{moment(member.updatedAt).format('HH:mm, DD MMMM YYYY')}</small>
        </Card>
    )
}

export default Members;