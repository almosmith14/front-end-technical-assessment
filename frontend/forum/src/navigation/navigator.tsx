
import { useState } from "react";
import { Link, Route, Routes, useMatch, useResolvedPath } from "react-router-dom";
import { useDispatch } from "react-redux";

import { Layout, Menu, Breadcrumb, Select, Typography } from 'antd';
import { CommentOutlined, UserOutlined } from '@ant-design/icons';

import Comments from "../components/comments";
import Members from "../components/members";

import setOrganisation from "../actions/organisation";

const { Header, Content, Footer, Sider } = Layout;
const { SubMenu } = Menu;
const { Option } = Select;
const { Text } = Typography
const AppNavigator = () => {
    const dispatch = useDispatch();
    const [collapsed, setCollapsed] = useState(false);

    const handleCompanyChange = (value: string) => {
        dispatch(setOrganisation(value));
    }



    return (
        <Layout style={{ minHeight: '100vh' }}>
            <Header className="header" style={{ padding: 0 }} >
                <img src="../logo.png" style={{ padding: 10, height: '100%' }} />
                <div style={{ float: 'right', paddingRight: 20 }}>
                    <Text style={{ color: 'white' }}>Company : </Text>
                    <Select defaultValue="fsociety" style={{ width: 120 }} onChange={handleCompanyChange}>
                        <Option value="fsociety">fsociety</Option>
                        <Option value="ecorp">ecorp</Option>
                        <Option value="error">error test</Option>
                    </Select>
                </div>
            </Header>
            <Layout>
                <Sider collapsible collapsed={collapsed} onCollapse={() => setCollapsed(!collapsed)}>

                    <Menu theme="dark" defaultSelectedKeys={[window.location.pathname]} mode="inline">
                        <Menu.Item key="/" icon={<CommentOutlined />}>
                            <Link to="/">Comments</Link>
                        </Menu.Item>
                        <Menu.Item key="/members" icon={<UserOutlined />}>
                            <Link to="/members">Members</Link>
                        </Menu.Item>
                    </Menu>
                </Sider>

                <Layout className="site-layout">

                    <Content style={{ margin: '16px' }}>
                        <Routes>
                            <Route path="/" element={<Comments />} />
                            <Route path="/members" element={<Members />} />
                        </Routes>
                    </Content>
                    <Footer style={{ textAlign: 'center' }}>4+UM</Footer>
                </Layout>
            </Layout>
        </Layout>
    )
}

export default AppNavigator;