import React from 'react';
import logo from './logo.svg';
import './App.css';
import AppNavigator from './navigation/navigator';
import { BrowserRouter } from 'react-router-dom';

function App() {
  return (
    <BrowserRouter>
      <AppNavigator />
    </BrowserRouter>

  );
}

export default App;
